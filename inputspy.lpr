program inputspy;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes, SysUtils, CustApp
  { you can add units after this },
  Windows, JwaWinUser;

type

  { TInputSpy }

  TInputSpy = class(TCustomApplication)
  protected
    procedure DoRun; override;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure WriteHelp; virtual;
  end;

{ TInputSpy }

procedure TInputSpy.DoRun;
var
  ErrorMsg: String;
  pInput:TInput;
  i: integer;
begin
  // parse parameters
  if HasOption('h', 'help') then begin
    Terminate;
    Exit;
  end;

  Sleep(1000);

  //create an input
  pInput.type_:=INPUT_KEYBOARD;
  pInput.ki.wVk:=$41;
  pInput.ki.wScan:=0;
  pInput.ki.time:=0;
  pInput.ki.dwExtraInfo:=0;

  for i:=1 to 10 do
  begin
  pInput.ki.dwFlags:=0;
  if SendInput(1,@pInput,SizeOf(pInput)) = 0 then
       Writeln('Something went wrong');

  pInput.ki.dwFlags:=KEYEVENTF_KEYUP;
  if SendInput(1,@pInput,SizeOf(pInput)) = 0 then
       Writeln('Something went wrong');
  end;

  // stop program loop
  Terminate;
end;

constructor TInputSpy.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  StopOnException:=True;
end;

destructor TInputSpy.Destroy;
begin
  inherited Destroy;
end;

procedure TInputSpy.WriteHelp;
begin
  { add your help code here }
  writeln('Usage: ', ExeName, ' -h');
end;

var
  Application: TInputSpy;
begin
  Application:=TInputSpy.Create(nil);
  Application.Title:='InputSpy';
  Application.Run;
  Application.Free;
end.

